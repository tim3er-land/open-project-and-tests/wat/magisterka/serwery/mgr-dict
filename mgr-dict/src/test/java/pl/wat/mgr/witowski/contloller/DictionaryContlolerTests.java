package pl.wat.mgr.witowski.contloller;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pl.wat.mgr.witowski.contloler.DictionaryContloler;
import pl.wat.mgr.witowski.dto.DictionaryItemDto;
import pl.wat.mgr.witowski.service.DictionarySqlService;

import java.util.Arrays;
import java.util.List;

public class DictionaryContlolerTests {
    private DictionaryContloler dictionaryContloler;
    private DictionarySqlService dictionarySqlService;

    @BeforeEach
    void setUp() {
        dictionarySqlService = Mockito.mock(DictionarySqlService.class);
        dictionaryContloler = new DictionaryContloler(dictionarySqlService);
    }

    @Test
    public void getDictionaryItemByCodeNull() {
        Mockito.when(dictionarySqlService.getDictionaryItemsByCode(Mockito.any(), Mockito.any())).thenReturn(null);
        ResponseEntity<List<DictionaryItemDto>> test = dictionaryContloler.getDictionaryItemsByCode("test", null);
        Assert.assertNotNull(test);
        Assert.assertEquals(test.getStatusCode(), HttpStatus.NO_CONTENT);
        Assert.assertNull(test.getBody());
    }

    @Test
    public void getDictionaryItemByCode() {
        Mockito.when(dictionarySqlService.getDictionaryItemsByCode(Mockito.any(), Mockito.any())).thenReturn(Arrays.asList(new DictionaryItemDto()));
        ResponseEntity<List<DictionaryItemDto>> test = dictionaryContloler.getDictionaryItemsByCode("test", null);
        Assert.assertNotNull(test);
        Assert.assertEquals(test.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(test.getBody());
    }
}
