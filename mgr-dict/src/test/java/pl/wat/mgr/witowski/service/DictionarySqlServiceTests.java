package pl.wat.mgr.witowski.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import pl.wat.mgr.witowski.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.dao.DicDictionaryEntity;
import pl.wat.mgr.witowski.dao.DicDictionaryItemEntity;
import pl.wat.mgr.witowski.dto.DictionaryItemDto;
import pl.wat.mgr.witowski.repository.DictonaryItemRepository;
import pl.wat.mgr.witowski.repository.DictonaryRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class DictionarySqlServiceTests {

    private DictonaryItemRepository dictonaryItemRepository;
    private DictonaryRepository dictonaryRepository;
    private DictionarySqlService dictionarySqlService;


    @Before
    public void setUp() throws Exception {
        dictonaryItemRepository = Mockito.mock(DictonaryItemRepository.class);
        dictonaryRepository = Mockito.mock(DictonaryRepository.class);
        dictionarySqlService = new DictionarySqlService(dictonaryItemRepository, dictonaryRepository);
    }

    @Test
    public void getDictionaryItemByCodeNullTest() {
        Mockito.when(dictonaryItemRepository.getAllDictionaryItemByCode(Mockito.any())).thenReturn(null);
        List<DictionaryItemDto> test = dictionarySqlService.getDictionaryItemsByCode("TEST", null);
        Assert.assertNull(test);
    }

    //
    @Test
    public void getDictionaryItemByCodeExceptionTest() {
        Mockito.when(dictonaryItemRepository.getAllDictionaryItemByCode(Mockito.any())).thenThrow(new NullPointerException());
        try {
            List<DictionaryItemDto> test = dictionarySqlService.getDictionaryItemsByCode("test", null);
        } catch (ApiExceptions ae) {
            Assert.assertNotNull(ae);
            Assert.assertNotNull(ae.getDescription());
            Assert.assertNotNull(ae.getCode());
            Assert.assertNotNull(ae.getStatus());
            Assert.assertEquals(ae.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
            Assert.assertEquals(ae.getCode(), "dict-500-001-001");
        }
    }

    @Test
    public void getDictionaryItemByCodeTest() {
        Mockito.when(dictonaryItemRepository.getAllDictionaryItemByCode(Mockito.any())).thenReturn(prepareDictionaryItem());
        List<DictionaryItemDto> test = dictionarySqlService.getDictionaryItemsByCode("test", null);
        Assert.assertNotNull(test);
        Assert.assertFalse(CollectionUtils.isEmpty(test));
    }

    private List<DicDictionaryItemEntity> prepareDictionaryItem() {
        return Arrays.asList(new DicDictionaryItemEntity(1, UUID.randomUUID().toString(), "TEST1", LocalDateTime.now(), "{}", LocalDateTime.now(), UUID.randomUUID().toString()
                , UUID.randomUUID().toString(), (short) 1, new DicDictionaryEntity()));

    }
}
