package pl.wat.mgr.witowski.dao;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "dic_dictionary_item", schema = "public", catalog = "postgres_mgr")
public class DicDictionaryItemEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "dic_dictionary_item_id")
    private long dicDictionaryItemId;
    @Basic
    @Column(name = "dic_dictionary_item_uid")
    private String dicDictionaryItemUid;
    @Basic
    @Column(name = "dic_dictionary_item_code")
    private String dicDictionaryItemCode;
    @Basic
    @Column(name = "dic_dictionary_item_insert_date")
    private LocalDateTime dicDictionaryItemInsertDate;
    @Basic
    @Column(name = "dic_dictionary_item_json")
    private String dicDictionaryItemJson;
    @Basic
    @Column(name = "dic_dictionary_item_modify_date")
    private LocalDateTime dicDictionaryItemModifyDate;
    @Basic
    @Column(name = "dic_dictionary_item_modify_by")
    private String dicDictionaryItemModifyBy;
    @Basic
    @Column(name = "dic_dictionary_item_insert_by")
    private String dicDictionaryItemInsertBy;
    @Basic
    @Column(name = "dic_dictionary_item_act")
    private short dicDictionaryItemAct;
    @ManyToOne
    @JoinColumn(name = "dic_dictionary_item_dictionary_id", referencedColumnName = "dic_dictionary_id", nullable = false)
    private DicDictionaryEntity dicDictionaryByDicDictionaryItemDictionaryId;

    public long getDicDictionaryItemId() {
        return dicDictionaryItemId;
    }

    public void setDicDictionaryItemId(long dicDictionaryItemId) {
        this.dicDictionaryItemId = dicDictionaryItemId;
    }

    public String getDicDictionaryItemUid() {
        return dicDictionaryItemUid;
    }

    public void setDicDictionaryItemUid(String dicDictionaryItemUid) {
        this.dicDictionaryItemUid = dicDictionaryItemUid;
    }

    public String getDicDictionaryItemCode() {
        return dicDictionaryItemCode;
    }

    public void setDicDictionaryItemCode(String dicDictionaryItemCode) {
        this.dicDictionaryItemCode = dicDictionaryItemCode;
    }

    public LocalDateTime getDicDictionaryItemInsertDate() {
        return dicDictionaryItemInsertDate;
    }

    public void setDicDictionaryItemInsertDate(LocalDateTime dicDictionaryItemInsertDate) {
        this.dicDictionaryItemInsertDate = dicDictionaryItemInsertDate;
    }

    public String getDicDictionaryItemJson() {
        return dicDictionaryItemJson;
    }

    public void setDicDictionaryItemJson(String dicDictionaryItemJson) {
        this.dicDictionaryItemJson = dicDictionaryItemJson;
    }

    public LocalDateTime getDicDictionaryItemModifyDate() {
        return dicDictionaryItemModifyDate;
    }

    public void setDicDictionaryItemModifyDate(LocalDateTime dicDictionaryItemModifyDate) {
        this.dicDictionaryItemModifyDate = dicDictionaryItemModifyDate;
    }

    public String getDicDictionaryItemModifyBy() {
        return dicDictionaryItemModifyBy;
    }

    public void setDicDictionaryItemModifyBy(String dicDictionaryItemModifyBy) {
        this.dicDictionaryItemModifyBy = dicDictionaryItemModifyBy;
    }

    public String getDicDictionaryItemInsertBy() {
        return dicDictionaryItemInsertBy;
    }

    public void setDicDictionaryItemInsertBy(String dicDictionaryItemInsertBy) {
        this.dicDictionaryItemInsertBy = dicDictionaryItemInsertBy;
    }

    public short getDicDictionaryItemAct() {
        return dicDictionaryItemAct;
    }

    public void setDicDictionaryItemAct(short dicDictionaryItemAct) {
        this.dicDictionaryItemAct = dicDictionaryItemAct;
    }

    public DicDictionaryItemEntity(long dicDictionaryItemId, String dicDictionaryItemUid, String dicDictionaryItemCode, LocalDateTime dicDictionaryItemInsertDate, String dicDictionaryItemJson, LocalDateTime dicDictionaryItemModifyDate, String dicDictionaryItemModifyBy, String dicDictionaryItemInsertBy, short dicDictionaryItemAct, DicDictionaryEntity dicDictionaryByDicDictionaryItemDictionaryId) {
        this.dicDictionaryItemId = dicDictionaryItemId;
        this.dicDictionaryItemUid = dicDictionaryItemUid;
        this.dicDictionaryItemCode = dicDictionaryItemCode;
        this.dicDictionaryItemInsertDate = dicDictionaryItemInsertDate;
        this.dicDictionaryItemJson = dicDictionaryItemJson;
        this.dicDictionaryItemModifyDate = dicDictionaryItemModifyDate;
        this.dicDictionaryItemModifyBy = dicDictionaryItemModifyBy;
        this.dicDictionaryItemInsertBy = dicDictionaryItemInsertBy;
        this.dicDictionaryItemAct = dicDictionaryItemAct;
        this.dicDictionaryByDicDictionaryItemDictionaryId = dicDictionaryByDicDictionaryItemDictionaryId;
    }

    public DicDictionaryItemEntity() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DicDictionaryItemEntity that = (DicDictionaryItemEntity) o;
        return dicDictionaryItemId == that.dicDictionaryItemId && dicDictionaryItemAct == that.dicDictionaryItemAct && Objects.equals(dicDictionaryItemUid, that.dicDictionaryItemUid) && Objects.equals(dicDictionaryItemCode, that.dicDictionaryItemCode) && Objects.equals(dicDictionaryItemInsertDate, that.dicDictionaryItemInsertDate) && Objects.equals(dicDictionaryItemJson, that.dicDictionaryItemJson) && Objects.equals(dicDictionaryItemModifyDate, that.dicDictionaryItemModifyDate) && Objects.equals(dicDictionaryItemModifyBy, that.dicDictionaryItemModifyBy) && Objects.equals(dicDictionaryItemInsertBy, that.dicDictionaryItemInsertBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dicDictionaryItemId, dicDictionaryItemUid, dicDictionaryItemCode, dicDictionaryItemInsertDate, dicDictionaryItemJson, dicDictionaryItemModifyDate, dicDictionaryItemModifyBy, dicDictionaryItemInsertBy, dicDictionaryItemAct);
    }

    public DicDictionaryEntity getDicDictionaryByDicDictionaryItemDictionaryId() {
        return dicDictionaryByDicDictionaryItemDictionaryId;
    }

    public void setDicDictionaryByDicDictionaryItemDictionaryId(DicDictionaryEntity dicDictionaryByDicDictionaryItemDictionaryId) {
        this.dicDictionaryByDicDictionaryItemDictionaryId = dicDictionaryByDicDictionaryItemDictionaryId;
    }
}
