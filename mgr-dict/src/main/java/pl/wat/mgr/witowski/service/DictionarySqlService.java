package pl.wat.mgr.witowski.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import pl.wat.mgr.witowski.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.dao.DicDictionaryItemEntity;
import pl.wat.mgr.witowski.dto.DictionaryItemDto;
import pl.wat.mgr.witowski.repository.DictonaryItemRepository;
import pl.wat.mgr.witowski.repository.DictonaryRepository;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class DictionarySqlService  {
    private final DictonaryItemRepository dictonaryItemRepository;
    private final DictonaryRepository dictonaryRepository;
    private final ObjectMapper objectMapper;

    public DictionarySqlService(DictonaryItemRepository dictonaryItemRepository, DictonaryRepository dictonaryRepository) {
        this.dictonaryItemRepository = dictonaryItemRepository;
        this.dictonaryRepository = dictonaryRepository;
        objectMapper = new ObjectMapper().registerModule(new JavaTimeModule()); // new module, NOT JSR310Module
    }

    public List<DictionaryItemDto> getDictionaryItemsByCode(String code, String locale) {
        List<DicDictionaryItemEntity> dictionaryItemDaos = null;
        try {
            dictionaryItemDaos = dictonaryItemRepository.getAllDictionaryItemByCode(code);
        } catch (Exception ex) {
            log.error("Error occured while get dictionry item with code: {}", code, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "dict-500-001-001", "dict-internal-error");
        }
        if (CollectionUtils.isEmpty(dictionaryItemDaos)) {
            return null;
        }
        return mapDictonaryItemDaoToDto(dictionaryItemDaos);
    }

    private List<DictionaryItemDto> mapDictonaryItemDaoToDto(List<DicDictionaryItemEntity> dictionaryItemDaos) {//TODO do poprawy
        return dictionaryItemDaos.stream().map(item -> {
            try {
                return DictionaryItemDto.builder()
                        .itemCode(item.getDicDictionaryItemCode())
                        .itemUid(item.getDicDictionaryItemUid())
                        .itemJson(objectMapper.readValue(item.getDicDictionaryItemJson(), new TypeReference<HashMap<String, Object>>() {
                        }))
                        .build();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return null;
            }
        }).collect(Collectors.toList());
    }

    public DictionaryItemDto getDictionaryItem(String code, String itemCode, String locale) {
        DicDictionaryItemEntity itemJson = null;
        try {
            if (StringUtils.isEmpty(locale)) {
                locale = "ENG";
            }
            itemJson = dictonaryItemRepository.getAllDictionaryItem(code, itemCode);

        } catch (Exception ex) {
            log.error("Error occured while get dictionry item with code: {}", code, ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "dict-500-002-001", "dict-internal-error");
        }
        if (itemJson == null) {
            log.warn("Can not find dictionary item with params (code: {}, itemCode: {})", code, itemCode);
            return null;
        }
        try {
            return DictionaryItemDto.builder()
                    .itemCode(itemJson.getDicDictionaryItemCode())
                    .itemUid(itemJson.getDicDictionaryItemUid())
                    .itemJson(objectMapper.readValue(itemJson.getDicDictionaryItemJson(), new TypeReference<HashMap<String, Object>>() {
                    }))
                    .build();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
