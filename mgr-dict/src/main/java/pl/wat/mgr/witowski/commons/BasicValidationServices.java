package pl.wat.mgr.witowski.commons;

import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import pl.wat.mgr.witowski.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.commons.exceptions.dto.ErrorDto;

import java.util.ArrayList;
import java.util.List;

public abstract class BasicValidationServices {

    private List<ErrorDto> errorDtoList;
    private String errorCode;
    private String errorDescription;


    public BasicValidationServices(String errorCode, String errorDescription) {
        this.errorDtoList = new ArrayList<>();
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public void validate() {
        if (CollectionUtils.isEmpty(errorDtoList)) {
            throw new ApiExceptions(HttpStatus.BAD_REQUEST, errorCode, errorDescription);
        }
    }

    protected void addNewErrorDto(String field, String message, String code) {
        errorDtoList.add(ErrorDto.builder()
                .code(code)
                .message(message)
                .field(field)
                .build());
    }
}
