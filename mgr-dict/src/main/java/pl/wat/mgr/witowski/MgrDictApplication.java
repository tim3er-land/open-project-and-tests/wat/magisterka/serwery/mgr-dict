package pl.wat.mgr.witowski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgrDictApplication {

    public static void main(String[] args) {
        SpringApplication.run(MgrDictApplication.class, args);
    }

}
