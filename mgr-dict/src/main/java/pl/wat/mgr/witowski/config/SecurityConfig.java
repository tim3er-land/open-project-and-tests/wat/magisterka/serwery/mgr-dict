package pl.wat.mgr.witowski.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.wat.mgr.witowski.security.GenerateNewTokenService;
import pl.wat.mgr.witowski.security.jwt.JwtAuthenticationEntryPoint;
import pl.wat.mgr.witowski.security.jwt.JwtTokenFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    private final GenerateNewTokenService generateNewTokenService;

    @Autowired
    public SecurityConfig(JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint, GenerateNewTokenService generateNewTokenService) {
        this.jwtAuthenticationEntryPoint = jwtAuthenticationEntryPoint;
        this.generateNewTokenService = generateNewTokenService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        JwtTokenFilter customFilter = new JwtTokenFilter();

        http.csrf().disable()
                // dont authenticate this particular request
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/dict/server/setting").permitAll()
                .and()
                .authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll()
                // all other requests need to be authenticated
                .anyRequest().authenticated().and().exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint);

        // Add a filter to validate the tokens with every request
        http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
    }
}

