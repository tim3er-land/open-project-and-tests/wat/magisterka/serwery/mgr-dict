package pl.wat.mgr.witowski.dao;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "dic_dictionary", schema = "public", catalog = "postgres_mgr")
public class DicDictionaryEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "dic_dictionary_id")
    private long dicDictionaryId;
    @Basic
    @Column(name = "dic_dictionary_uid")
    private String dicDictionaryUid;
    @Basic
    @Column(name = "dic_dictionary_code")
    private String dicDictionaryCode;
    @Basic
    @Column(name = "dic_dictionary_description")
    private String dicDictionaryDescription;
    @Basic
    @Column(name = "dic_dictionary_insert_date")
    private LocalDateTime dicDictionaryInsertDate;
    @Basic
    @Column(name = "dic_dictionary_modify_date")
    private LocalDateTime dicDictionaryModifyDate;
    @Basic
    @Column(name = "dic_dictionary_modify_by")
    private String dicDictionaryModifyBy;
    @Basic
    @Column(name = "dic_dictionary_insert_by")
    private String dicDictionaryInsertBy;
    @Basic
    @Column(name = "dic_dictionary_act")
    private short dicDictionaryAct;
    @OneToMany(mappedBy = "dicDictionaryByDicDictionaryItemDictionaryId")
    private Collection<DicDictionaryItemEntity> dicDictionaryItemsByDicDictionaryId;

    public long getDicDictionaryId() {
        return dicDictionaryId;
    }

    public void setDicDictionaryId(long dicDictionaryId) {
        this.dicDictionaryId = dicDictionaryId;
    }

    public String getDicDictionaryUid() {
        return dicDictionaryUid;
    }

    public void setDicDictionaryUid(String dicDictionaryUid) {
        this.dicDictionaryUid = dicDictionaryUid;
    }

    public String getDicDictionaryCode() {
        return dicDictionaryCode;
    }

    public void setDicDictionaryCode(String dicDictionaryCode) {
        this.dicDictionaryCode = dicDictionaryCode;
    }

    public String getDicDictionaryDescription() {
        return dicDictionaryDescription;
    }

    public void setDicDictionaryDescription(String dicDictionaryDescription) {
        this.dicDictionaryDescription = dicDictionaryDescription;
    }

    public LocalDateTime getDicDictionaryInsertDate() {
        return dicDictionaryInsertDate;
    }

    public void setDicDictionaryInsertDate(LocalDateTime dicDictionaryInsertDate) {
        this.dicDictionaryInsertDate = dicDictionaryInsertDate;
    }

    public LocalDateTime getDicDictionaryModifyDate() {
        return dicDictionaryModifyDate;
    }

    public void setDicDictionaryModifyDate(LocalDateTime dicDictionaryModifyDate) {
        this.dicDictionaryModifyDate = dicDictionaryModifyDate;
    }

    public String getDicDictionaryModifyBy() {
        return dicDictionaryModifyBy;
    }

    public void setDicDictionaryModifyBy(String dicDictionaryModifyBy) {
        this.dicDictionaryModifyBy = dicDictionaryModifyBy;
    }

    public String getDicDictionaryInsertBy() {
        return dicDictionaryInsertBy;
    }

    public void setDicDictionaryInsertBy(String dicDictionaryInsertBy) {
        this.dicDictionaryInsertBy = dicDictionaryInsertBy;
    }

    public short getDicDictionaryAct() {
        return dicDictionaryAct;
    }

    public void setDicDictionaryAct(short dicDictionaryAct) {
        this.dicDictionaryAct = dicDictionaryAct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DicDictionaryEntity that = (DicDictionaryEntity) o;
        return dicDictionaryId == that.dicDictionaryId && dicDictionaryAct == that.dicDictionaryAct && Objects.equals(dicDictionaryUid, that.dicDictionaryUid) && Objects.equals(dicDictionaryCode, that.dicDictionaryCode) && Objects.equals(dicDictionaryDescription, that.dicDictionaryDescription) && Objects.equals(dicDictionaryInsertDate, that.dicDictionaryInsertDate) && Objects.equals(dicDictionaryModifyDate, that.dicDictionaryModifyDate) && Objects.equals(dicDictionaryModifyBy, that.dicDictionaryModifyBy) && Objects.equals(dicDictionaryInsertBy, that.dicDictionaryInsertBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dicDictionaryId, dicDictionaryUid, dicDictionaryCode, dicDictionaryDescription, dicDictionaryInsertDate, dicDictionaryModifyDate, dicDictionaryModifyBy, dicDictionaryInsertBy, dicDictionaryAct);
    }

    public Collection<DicDictionaryItemEntity> getDicDictionaryItemsByDicDictionaryId() {
        return dicDictionaryItemsByDicDictionaryId;
    }

    public void setDicDictionaryItemsByDicDictionaryId(Collection<DicDictionaryItemEntity> dicDictionaryItemsByDicDictionaryId) {
        this.dicDictionaryItemsByDicDictionaryId = dicDictionaryItemsByDicDictionaryId;
    }
}
