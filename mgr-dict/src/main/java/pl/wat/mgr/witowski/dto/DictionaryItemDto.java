package pl.wat.mgr.witowski.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DictionaryItemDto {

    private String itemUid;
    private String itemCode;
    private HashMap<String, Object> itemJson;
}
