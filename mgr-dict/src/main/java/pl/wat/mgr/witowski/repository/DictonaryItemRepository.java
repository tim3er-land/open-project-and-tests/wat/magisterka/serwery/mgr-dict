package pl.wat.mgr.witowski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.wat.mgr.witowski.dao.DicDictionaryItemEntity;

import java.util.List;

@Repository
public interface DictonaryItemRepository extends JpaRepository<DicDictionaryItemEntity, Long> {

    @Query("select di from DicDictionaryItemEntity di join di.dicDictionaryByDicDictionaryItemDictionaryId dd" +
            " where upper(dd.dicDictionaryCode) like upper(:code) ")
    List<DicDictionaryItemEntity> getAllDictionaryItemByCode(@Param("code") String code);

    @Query("select di from DicDictionaryItemEntity di join di.dicDictionaryByDicDictionaryItemDictionaryId dd" +
            " where upper(dd.dicDictionaryCode) like upper(:code) and upper(di.dicDictionaryItemCode) like upper(:itemCode) ")
    DicDictionaryItemEntity getAllDictionaryItem(@Param("code") String code, @Param("itemCode") String itemCode);
}
