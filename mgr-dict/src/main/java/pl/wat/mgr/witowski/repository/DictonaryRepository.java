package pl.wat.mgr.witowski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.wat.mgr.witowski.dao.DicDictionaryEntity;

@Repository
public interface DictonaryRepository extends JpaRepository<DicDictionaryEntity, Long> {


    @Query("select dd from DicDictionaryEntity dd where upper(dd.dicDictionaryCode) = upper(:code) ")
    DicDictionaryEntity getDictionaryByCode(@Param("code") String code);
}
