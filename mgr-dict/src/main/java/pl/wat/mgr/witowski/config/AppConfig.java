package pl.wat.mgr.witowski.config;

import com.google.common.cache.CacheBuilder;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.CacheManager;
import org.springframework.cache.guava.GuavaCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import pl.wat.mgr.witowski.security.GenerateNewTokenService;

import javax.annotation.PostConstruct;
import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Log4j2
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
@EntityScan("pl.wat.mgr.witowski.dao")
@EnableJpaRepositories("pl.wat.mgr.witowski.repository")
@ComponentScan({"pl.wat.mgr.witowski"})
public class AppConfig {
    @Autowired
    GenerateNewTokenService generateNewTokenService;

    @PostConstruct
    public void init() {
        try {
            log.debug("send Order To Generate NewKey ");
            generateNewTokenService.sendOrderToGenerateNewKey();
        } catch (Exception ex) {
            log.error("Errorr occured while generate key at startup ", ex);
        }
    }

    @Bean
    public RestTemplate restTemplate() {
        log.debug("Creating bean restTemplate");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                if (connection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) connection).setHostnameVerifier((s, sslSession) -> true);
                }
                super.prepareConnection(connection, httpMethod);
            }
        });
        return restTemplate;
    }

    @Bean
    @Primary
    public CacheManager cacheManager() {
        log.debug("creating bean cacheManager");
        SimpleCacheManager cacheManager = new SimpleCacheManager();

        GuavaCache tokenCache = new GuavaCache("token_cache", CacheBuilder.newBuilder()
                .expireAfterWrite(60, TimeUnit.SECONDS)
                .build());

        GuavaCache dictionaryCache = new GuavaCache("dictionary_cache", CacheBuilder.newBuilder()
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build());
        cacheManager.setCaches(Arrays.asList(tokenCache, dictionaryCache));
        return cacheManager;
    }

}
