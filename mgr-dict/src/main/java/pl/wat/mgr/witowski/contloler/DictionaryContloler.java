package pl.wat.mgr.witowski.contloler;


import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import pl.wat.mgr.witowski.dto.DictionaryItemDto;
import pl.wat.mgr.witowski.service.DictionarySqlService;

import java.util.List;

@Log4j2
@RestController
public class DictionaryContloler {

    private final DictionarySqlService dictionaryService;

    @Autowired
    public DictionaryContloler(DictionarySqlService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }


    @GetMapping("dict/{code}/list")
    @PreAuthorize(value = "hasAnyRole('INNER_USER','USER')")
    public ResponseEntity<List<DictionaryItemDto>> getDictionaryItemsByCode(@PathVariable("code") String code, @RequestHeader(value = "Locale", required = false) String locale) {
        log.info("start method getDictionaryItemsByCode with code: {}", code);
        List<DictionaryItemDto> dictionaryItemDtos = dictionaryService.getDictionaryItemsByCode(code, locale);
        if (CollectionUtils.isEmpty(dictionaryItemDtos)) {
            log.info("start method getDictionaryItemsByCode with code: {}, no content", code);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        log.info("start method getDictionaryItemsByCode with code: {}, return size", code, dictionaryItemDtos.size());
        return new ResponseEntity<>(dictionaryItemDtos, HttpStatus.OK);
    }

    @GetMapping("dict/{code}/item/{itemCode}")
    @PreAuthorize(value = "hasAnyRole('INNER_USER','USER')")
    public ResponseEntity<DictionaryItemDto> getDictionaryItem(@PathVariable("code") String code, @PathVariable("itemCode") String itemCode, @RequestHeader(value = "Locale", required = false) String locale) {
        log.info("start method getDictionaryItem with code: {}", code);
        DictionaryItemDto dictionaryItemDto = dictionaryService.getDictionaryItem(code, itemCode, locale);
        if (dictionaryItemDto == null) {
            log.info("end method getDictionaryItem with code: {}, no content", code);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        log.info("end method getDictionaryItem with code: {},", code);
        return new ResponseEntity<>(dictionaryItemDto, HttpStatus.OK);
    }
}
